## Objectives

1. Studenst should explore CLI commands and get familiar with the basic commands

## Resources

* [Reference Sheet for Advanced CLI](https://files.fosswire.com/2007/08/fwunixref.pdf)
* [Tutorial on CLI](https://gist.github.com/aviflombaum/9d6f7448119bae3a24ee)
* [Command Line for Beginners](https://lifehacker.com/5633909/who-needs-a-mouse-learn-to-use-the-command-line-for-almost-anything)
<p class='util--hide'>View <a href='https://learn.co/lessons/kwk-teachers-guide-intro-to-cli'>KWK Teachers Guide Intro to CLI</a> on Learn.co and start learning to code for free.</p>
